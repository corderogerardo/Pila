/*
 * Pila.h
 *
 *  Created on: 25/5/2015
 *      Author: Gerardo Cordero_2
 */

#ifndef PILA_H_
#define PILA_H_
#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>

#include "Node.cpp"

using namespace std;

template <class T>

class Pila {
private:
	Node<T> *m_inicio, *m_fin;
public:
	Pila();
	~Pila();

	 void agregar(T);
	 int retirar();
	  int frecuencia(int pos);
	  int cuantos_hay();
	  bool pilaVacia();
	  void imprimir();
	  void eliminarImpares();


	 int mayor();
	 int menor();

};

#endif /* PILA_H_ */

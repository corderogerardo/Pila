#include "Node.h"

// Constructor por defecto
template<typename T>

Node<T>::Node()
{
    data = NULL;
    siguiente = NULL;
    anterior = NULL;
}

// Constructor por parámetro
template<typename T>
Node<T>::Node(T data_)
{
    data = data_;
    siguiente = NULL;
    anterior = NULL;
}

// Eliminar todos los Nodos
template<typename T>
void Node<T>::delete_all()
{
    if (siguiente)
        siguiente->delete_all();
    delete this;
}

// Imprimir un Nodo
template<typename T>
void Node<T>::print()
{
    //cout << "Node-> " << "Dato: " << dato << " Direcion: " << this << " Siguiente: " << next << endl;
    cout << data << "-> ";
}


template<typename T>
Node<T>::~Node() {


}

/*
 * Pila.cpp
 *
 *  Created on: 25/5/2015
 *      Author: Gerardo Cordero_2
 */

#include "Pila.h"
using namespace std;

template<typename T>
Pila<T>::Pila() {
	// TODO Auto-generated constructor stub
	 m_inicio = NULL;
	 m_fin = NULL;
}

// Insertar al inicio
template<typename T>
void Pila<T>::agregar(T data_)
{
    Node<T> *nuevo_nodo = new Node<T> (data_);
    Node<T> *auxNodo = new Node<T> (data_);

    auxNodo=m_inicio;
    if (m_inicio == NULL) {
    	m_inicio = nuevo_nodo;
    	m_fin = nuevo_nodo;
    	return;
    } else {
    	nuevo_nodo->siguiente=auxNodo;
    	nuevo_nodo->anterior= auxNodo->anterior;
    	auxNodo->anterior=nuevo_nodo;
    	m_inicio = nuevo_nodo;
    }
}

template<typename T>
int Pila<T>::retirar()
{
	Node<T> *a_eliminar = m_inicio;
	int valorAlInicio=0;
	if(cuantos_hay()>0){
		valorAlInicio=m_inicio->data;
		m_inicio=a_eliminar->siguiente;
		delete a_eliminar;
	return valorAlInicio;
	}
}

template<typename T>
void Pila<T>::eliminarImpares()
{
	Node<T> *a_eliminar = m_inicio;
	Node<T> *auxNodo = m_inicio;

	int valorAlInicio=0;
	valorAlInicio = m_inicio->data;
	int impar=0;
	impar=valorAlInicio%2;
		while((auxNodo!=NULL) && (impar>0))
		{
			m_inicio=auxNodo->siguiente;
			delete auxNodo;
		}
		while(auxNodo!=NULL)
		{
			a_eliminar=auxNodo->siguiente;
			if((a_eliminar->data%2)>0)
			{
				auxNodo->siguiente=a_eliminar->siguiente;
				delete a_eliminar;
			}
			auxNodo=auxNodo->siguiente;
		}


}




template<typename T>
int Pila<T>::frecuencia(int pos)
{
	Node<T> *auxNode = m_inicio;
	Node<T> *tempo = m_inicio;
	int frecuencia=0;

	frecuencia=0;
		while(auxNode!=NULL)
		{
			int valor=0;
				for(int i=0;i<pos;i++)
				{
					m_inicio=m_inicio->siguiente;
				}
				valor=m_inicio->data;
				m_inicio=tempo;

		if(auxNode->data==valor)
		{
			frecuencia++;
		}
		auxNode=auxNode->siguiente;
		}
		return frecuencia;
}




template<typename T>
bool Pila<T>::pilaVacia(){
	 Node<T> *aux_node = m_inicio;
		int cantidad=0;
			if(m_inicio == NULL)
			{
		      return true;
			}
			else{
				return false;
			}

}

template<typename T>
int Pila<T>::cuantos_hay()
{
	 Node<T> *aux_node = m_inicio;

	int cantidad=0;

		if(m_inicio == NULL){
	   	puts("No hay elementos en la lista");
	      return 0;
	   }
	   else{
	   	while(m_inicio!=NULL){
	      	cantidad ++;
	      	m_inicio = m_inicio->siguiente;
	      }
	   	m_inicio=aux_node;
	      return(cantidad);
	   }
}

// Imprimir la cola
template<typename T>
void Pila<T>::imprimir()
{
    Node<T> *temp = m_inicio;
    if (!m_inicio) {
        cout << "La Lista est� vac�a " << endl;
    } else {
        while (temp) {
            temp->print();
            if (!temp->siguiente) cout << "NULL";
                temp = temp->siguiente;
        }
  }
  cout << endl << endl;
}

// Mayor
template<typename T>
int Pila<T>::mayor()
{
	int el_mayor;
	Node<T> *lugar = m_inicio;
	el_mayor = lugar->data;
	while(lugar->siguiente!=NULL)
	{
		if(lugar->data>el_mayor)
		{
			el_mayor=lugar->data;

		}
		lugar=lugar->siguiente;
	}
	return el_mayor;
}
// Menor
template<typename T>
int Pila<T>::menor()
{
	int el_menor;
		Node<T> *lugar = m_inicio;
		el_menor = lugar->data;
		while(lugar->siguiente!=NULL)
		{
			if(lugar->data<el_menor)
			{
				el_menor=lugar->data;

			}
			lugar=lugar->siguiente;
		}
		return el_menor;

}


template<typename T>
Pila<T>::~Pila() {

}

